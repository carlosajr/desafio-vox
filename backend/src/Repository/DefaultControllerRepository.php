<?php

namespace App\Repository;

use App\Entity\DefaultController;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DefaultController|null find($id, $lockMode = null, $lockVersion = null)
 * @method DefaultController|null findOneBy(array $criteria, array $orderBy = null)
 * @method DefaultController[]    findAll()
 * @method DefaultController[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DefaultControllerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DefaultController::class);
    }

    // /**
    //  * @return DefaultController[] Returns an array of DefaultController objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DefaultController
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
