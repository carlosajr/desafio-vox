import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms/';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/template/header/header.component';
import { FooterComponent } from './components/template/footer/footer.component';
import { NavComponent } from './components/template/nav/nav.component';
import { HomeComponent } from './views/home/home.component';
import { CompanyCrudComponent } from './views/company-crud/company-crud.component';
import { PartnerCrudComponent } from './views/partner-crud/partner-crud.component';
import { CompanyCreateComponent } from './components/company/company-create/company-create.component';
import { PartnerCreateComponent } from './components/partner/partner-create/partner-create.component'
import { CompanyReadComponent } from './components/company/company-read/company-read.component';
import { PartnerReadComponent } from './components/partner/partner-read/partner-read.component';
import { CompayReadExampleComponent } from './components/company/compay-read-example/compay-read-example.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { CompanyUpdateComponent } from './components/company/company-update/company-update.component';
import { PartnerUpdateComponent } from './components/partner/partner-update/partner-update.component';
import { CompanyDeleteComponent } from './components/company/company-delete/company-delete.component';
import { PartnerDeleteComponent } from './components/partner/partner-delete/partner-delete.component';
import { PartnerShowComponent } from './components/partner/partner-show/partner-show.component';
import { CompanyShowComponent } from './components/company/company-show/company-show.component';
import { CompanyAddPartnerComponent } from './components/company/company-add-partner/company-add-partner.component';
import { CompanyShowPartnersComponent } from './components/company/company-show-partners/company-show-partners.component'


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent,
    HomeComponent,
    CompanyCrudComponent,
    PartnerCrudComponent,
    CompanyCreateComponent,
    PartnerCreateComponent,
    CompanyReadComponent,
    PartnerReadComponent,
    CompayReadExampleComponent,
    CompanyUpdateComponent,
    PartnerUpdateComponent,
    CompanyDeleteComponent,
    PartnerDeleteComponent,
    PartnerShowComponent,
    CompanyShowComponent,
    CompanyAddPartnerComponent,
    CompanyShowPartnersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDividerModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
