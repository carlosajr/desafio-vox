import { HeaderService } from './../../components/template/header/header.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-partner-crud',
  templateUrl: './partner-crud.component.html',
  styleUrls: ['./partner-crud.component.css']
})
export class PartnerCrudComponent implements OnInit {

  constructor(private router: Router,  private headerService: HeaderService) {
    headerService.headerData = {
      title: 'Cadastro de Sócios',
      icon: '  groups  ',
      routeUrl: '/partners'
    }    
   }

  ngOnInit(): void {
  }

  navigateToPartnerCreate(): void {
    this.router.navigate(['/partners/create']) 
  }

}
