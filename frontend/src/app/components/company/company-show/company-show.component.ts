import { CompanyService } from './../company.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Company } from './../company.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-show',
  templateUrl: './company-show.component.html',
  styleUrls: ['./company-show.component.css']
})
export class CompanyShowComponent implements OnInit {

  company: Company

  constructor(private companyService: CompanyService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.companyService.show(id).subscribe(company => {
      this.company = company['data']
      console.log(this.company);
    })
  }

  cancel():void {
    this.router.navigate(['/companies']) 
  }

}
