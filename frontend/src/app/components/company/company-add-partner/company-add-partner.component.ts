import { Partners } from './../../partner/partner.model';
import { PartnerService } from './../../partner/partner.service';
import { Company } from './../company.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from './../company.service';
import { Component, OnInit } from '@angular/core';
import { Partner } from '../../partner/partner.model';

@Component({
  selector: 'app-company-add-partner',
  templateUrl: './company-add-partner.component.html',
  styleUrls: ['./company-add-partner.component.css']
})
export class CompanyAddPartnerComponent implements OnInit {

  company: Company
  selectedValue: Partner
  partners: Partners[]

  constructor(private companyService: CompanyService,
              private partnerService: PartnerService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.companyService.show(id).subscribe(company => {
      this.company = company['data']
      console.log(this.company);
    })

    this.partnerService.read().subscribe(partners => {
      this.partners = partners['data']
      console.log(this.partners)
    })

  }

  addPartner():void {
    const idCompany = this.company.id;
    const idPatern = this.selectedValue;
   
    this.companyService.addPartnerToCompany(idCompany, idPatern).subscribe(() =>{
      this.companyService.showMessage('Socio vinculado a Empresa com Sucesso')
      const url = `/companies/${this.company.id}/showPartners`
      this.router.navigate([url]) 
    });
  }

  cancel():void {
    const url = `/companies/${this.company.id}/showPartners`
    this.router.navigate([url]) 
  }

}
