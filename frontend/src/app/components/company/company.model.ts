export interface Company {
    id?: number
    name: string
    fantasy: string
    cnpj: string
    port: string
    nature: string
    phone: string
    mail: string
    cep: string
    state: string
    city: string
    district: string
    street: string
    number: number
}