import { Company } from './../company.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from './../company.service';
import { Component, OnInit } from '@angular/core';
import { Partner } from '../../partner/partner.model';

@Component({
  selector: 'app-company-show-partners',
  templateUrl: './company-show-partners.component.html',
  styleUrls: ['./company-show-partners.component.css']
})
export class CompanyShowPartnersComponent implements OnInit {

  company: Company
  partners: Partner[]
  displayedColumns = ['id', 'name', 'cpf', 'mail', 'action']

  constructor(private companyService: CompanyService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.companyService.show(id).subscribe(company => {
      this.company = company['data']
      console.log(this.company);
    })

    this.companyService.showPartners(id).subscribe(partners => {
      this.partners = partners['data']
      console.log(this.partners)
    })

  }

  addPartner():void {
    const url = `/companies/${this.company.id}/addPartner`
    this.router.navigate([url]) 
  }

  removePartner(idPartner: string):void {
    const idCompany = this.company.id;

    this.companyService.removePartnerToCompany(idCompany, idPartner).subscribe(() =>{
      this.companyService.showMessage('Socio desvinculado da Empresa com Sucesso')
      this.router.navigate(['/companies']) 
    });
  }

  cancel():void {
    this.router.navigate(['/companies']) 
  }

}
