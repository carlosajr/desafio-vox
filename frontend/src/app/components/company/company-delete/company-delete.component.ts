import { Company } from './../company.model';
import { CompanyService } from './../company.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-delete',
  templateUrl: './company-delete.component.html',
  styleUrls: ['./company-delete.component.css']
})
export class CompanyDeleteComponent implements OnInit {

  company: Company

  constructor(private companyService: CompanyService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.companyService.show(id).subscribe(company => {
      this.company = company['data']
      console.log(this.company);
    })
  }

  deleteCompany():void {
    this.companyService.delete(this.company.id).subscribe(() => {
      this.companyService.showMessage('Empresa removida com Sucesso');
      this.router.navigate(['/companies']) 
    })
  }

  cancel():void {
    this.router.navigate(['/companies']) 
  }

}
