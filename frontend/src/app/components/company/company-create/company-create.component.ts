import { Company } from './../company.model';
import { CompanyService } from './../company.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-create',
  templateUrl: './company-create.component.html',
  styleUrls: ['./company-create.component.css']
})
export class CompanyCreateComponent implements OnInit {

  company: Company = {
    name: '',
    fantasy: '',
    cnpj: '',
    port:'MEI',
    nature:'EI',
    phone:'',
    mail:'',
    cep:'',
    state:'',
    city:'',
    district:'',
    street:'',
    number: null
  }

  constructor(private companyService: CompanyService,
              private router: Router) { }

  ngOnInit(): void {
    
  }

  createCompany():void {
    this.companyService.create(this.company).subscribe(() =>{
      this.companyService.showMessage('Empresa Cadastrada com Sucesso')
      this.router.navigate(['/companies']) 
    })
  } 

  cancel():void {
    this.router.navigate(['/companies']) 
  }


  
}
