import { catchError, map } from 'rxjs/operators';
import { Company } from './company.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EMPTY, Observable } from 'rxjs';
import { Partner } from '../partner/partner.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  baseUrl = "http://desafiovox.cvmakers.com.br:8000/companys";

  constructor(private snackBar: MatSnackBar,
              private http: HttpClient) { }

  showMessage(msg: string, isError: boolean = false):void {
    this.snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] :['msg-success']
    })
  }

  read(): Observable<Company[]> {
    return this.http.get<Company[]>(this.baseUrl)
  }

  create(company: Company): Observable<Company> {
    const url = `${this.baseUrl}/`
    return this.http.post<Company>(url, company).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    )
  }

  show(id: string): Observable<Company> {
    const url = `${this.baseUrl}/${id}`
    return this.http.get<Company>(url)
  }

  update(company: Company): Observable<Company> {
    const url = `${this.baseUrl}/${company.id}`
    return this.http.put<Company>(url, company)
  }

  delete(id: number): Observable<Company> {
    const url = `${this.baseUrl}/${id}`
    return this.http.delete<Company>(url)
  }

  errorHandler(e: any): Observable<any> {
    this.showMessage('Ocorreu um erro', true)
    return EMPTY
  }

  showPartners(id: string): Observable<Partner> {
    const url = `${this.baseUrl}/${id}/partners/`
    return this.http.get<Partner>(url)
  }

  addPartnerToCompany(idCompany: number, idPartner): Observable<Company> {
    const url = `${this.baseUrl}/${idCompany}/partners/${idPartner}`
    return this.http.post<Company>(url, '{"data":"OK"}')
  }

  removePartnerToCompany(idCompany: number, idPartner): Observable<Company> {
    const url = `${this.baseUrl}/${idCompany}/partners/${idPartner}`
    return this.http.delete<Company>(url)
  }

}
