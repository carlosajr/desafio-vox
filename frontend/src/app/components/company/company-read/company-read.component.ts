import { CompanyService } from './../company.service';
import { Company } from './../company.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-read',
  templateUrl: './company-read.component.html',
  styleUrls: ['./company-read.component.css']
})
export class CompanyReadComponent implements OnInit {

  companies: Company[]
  displayedColumns = ['id', 'name', 'fantasy', 'cnpj', 'action']

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {

    this.companyService.read().subscribe(companies => {
      this.companies = companies
      console.log(this.companies)
    })

  }

}
