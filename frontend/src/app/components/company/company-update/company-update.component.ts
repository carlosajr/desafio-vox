import { Company } from './../company.model';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from './../company.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-update',
  templateUrl: './company-update.component.html',
  styleUrls: ['./company-update.component.css']
})
export class CompanyUpdateComponent implements OnInit {

  company: Company

  constructor(private companyService: CompanyService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.companyService.show(id).subscribe(company => {
      this.company = company['data']
      console.log(this.company);
    })
  }

  updateCompany():void {
    this.companyService.update(this.company).subscribe(() => {
      this.companyService.showMessage('Empresa atualizada com Sucesso');
      this.router.navigate(['/companies']);
    })
  }

  cancel():void {
    this.router.navigate(['/companies']) 
  }

}
