export interface Partner {
    id?: number
    name: string
    cpf: string
    phone: string
    mail: string
    cep: string
    state: string
    city: string
    district: string
    street: string
    number: number
}

export interface Partners {
    id?: number
    name: string
    cpf: string
    phone: string
    mail: string
    cep: string
    state: string
    city: string
    district: string
    street: string
    number: number
}