import { map, catchError } from 'rxjs/operators';
import { Partner } from './partner.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EMPTY, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  baseUrl = "http://desafiovox.cvmakers.com.br:8000/partners";

  constructor(private snackBar: MatSnackBar,
              private http: HttpClient) { }

  showMessage(msg: string, isError: boolean = false):void {
    this.snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] :['msg-success']
    })
  }

  read(): Observable<Partner[]> {
    return this.http.get<Partner[]>(this.baseUrl)
  }

  create(partner: Partner): Observable<Partner> {
    const url = `${this.baseUrl}/`
    return this.http.post<Partner>(url, partner).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    )
  }

  show(id: string): Observable<Partner> {
    const url = `${this.baseUrl}/${id}`
    return this.http.get<Partner>(url)
  }

  update(partner: Partner): Observable<Partner> {
    const url = `${this.baseUrl}/${partner.id}`
    return this.http.put<Partner>(url, partner)
  }

  delete(id: number): Observable<Partner> {
    const url = `${this.baseUrl}/${id}`
    return this.http.delete<Partner>(url)
  }

  errorHandler(e: any): Observable<any> {
    this.showMessage('Ocorreu um erro', true)
    return EMPTY
  }
  
}
