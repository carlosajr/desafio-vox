import { Router, ActivatedRoute } from '@angular/router';
import { PartnerService } from './../partner.service';
import { Component, OnInit } from '@angular/core';
import { Partner } from '../partner.model';

@Component({
  selector: 'app-partner-update',
  templateUrl: './partner-update.component.html',
  styleUrls: ['./partner-update.component.css']
})
export class PartnerUpdateComponent implements OnInit {

  partner: Partner

  constructor(private partnerService: PartnerService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.partnerService.show(id).subscribe(partner => {
      this.partner = partner['data']
      console.log(this.partner);
    })
  }

  updatePartner():void {
    this.partnerService.update(this.partner).subscribe(() => {
      this.partnerService.showMessage('Sócio atualizado com Sucesso');
      this.router.navigate(['/partners']);
    })
  }

  cancel():void {
    this.router.navigate(['/partners']) 
  }

}
