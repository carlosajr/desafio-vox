import { Partner } from '../partner.model';
import { PartnerService } from './../partner.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-partner-delete',
  templateUrl: './partner-delete.component.html',
  styleUrls: ['./partner-delete.component.css']
})
export class PartnerDeleteComponent implements OnInit {

  partner: Partner

  constructor(private partnerService: PartnerService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.partnerService.show(id).subscribe(partner => {
      this.partner = partner['data']
      console.log(this.partner);
    })
  }

  deletePartner():void {
    this.partnerService.delete(this.partner.id).subscribe(() => {
      this.partnerService.showMessage('Sócio removido com Sucesso');
      this.router.navigate(['/partners']) 
    })
  }

  cancel():void {
    this.router.navigate(['/partners']) 
  }

}
