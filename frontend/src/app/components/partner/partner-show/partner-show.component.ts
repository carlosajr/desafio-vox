import { PartnerService } from './../partner.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Partner } from '../partner.model';

@Component({
  selector: 'app-partner-show',
  templateUrl: './partner-show.component.html',
  styleUrls: ['./partner-show.component.css']
})
export class PartnerShowComponent implements OnInit {

  partner: Partner

  constructor(private partnerService: PartnerService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.partnerService.show(id).subscribe(partner => {
      this.partner = partner['data']
      console.log(this.partner);
    })
  }

  cancel():void {
    this.router.navigate(['/partners']) 
  }

}
