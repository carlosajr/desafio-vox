import { PartnerService } from './../partner.service';
import { Component, OnInit } from '@angular/core';
import { Partner } from '../partner.model';

@Component({
  selector: 'app-partner-read',
  templateUrl: './partner-read.component.html',
  styleUrls: ['./partner-read.component.css']
})
export class PartnerReadComponent implements OnInit {

  partners: Partner[]
  displayedColumns = ['id', 'name', 'cpf', 'mail', 'action']

  constructor(private partnerService: PartnerService) { }

  ngOnInit(): void {

    this.partnerService.read().subscribe(partners => {
      this.partners = partners
      console.log(partners[0])  
    })

  }

}
