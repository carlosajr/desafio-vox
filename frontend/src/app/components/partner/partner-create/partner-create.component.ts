import { PartnerService } from './../partner.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Partner } from '../partner.model';

@Component({
  selector: 'app-partner-create',
  templateUrl: './partner-create.component.html',
  styleUrls: ['./partner-create.component.css']
})
export class PartnerCreateComponent implements OnInit {

  partner: Partner = {
    name: '',
    cpf: '',
    phone:'',
    mail:'',
    cep:'',
    state:'',
    city:'',
    district:'',
    street:'',
    number: null
  }

  constructor(private partnerService: PartnerService,
              private router: Router) { }

  ngOnInit(): void {
  }

  createPartner():void {
    this.partnerService.create(this.partner).subscribe(() => {
      this.partnerService.showMessage('Sócio Cadastrado com Sucesso')  
      this.router.navigate(['/partners']) 
    })

    this.partnerService.showMessage('Sócio Cadastrado com Sucesso')
  } 

  cancel():void {
    this.router.navigate(['/partners']) 
  }

}
