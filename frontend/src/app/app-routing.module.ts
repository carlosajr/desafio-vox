import { CompanyAddPartnerComponent } from './components/company/company-add-partner/company-add-partner.component';
import { CompanyShowPartnersComponent } from './components/company/company-show-partners/company-show-partners.component';
import { PartnerShowComponent } from './components/partner/partner-show/partner-show.component';
import { CompanyShowComponent } from './components/company/company-show/company-show.component';
import { CompanyDeleteComponent } from './components/company/company-delete/company-delete.component';
import { PartnerDeleteComponent } from './components/partner/partner-delete/partner-delete.component';
import { PartnerUpdateComponent } from './components/partner/partner-update/partner-update.component';
import { CompanyUpdateComponent } from './components/company/company-update/company-update.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './views/home/home.component'

import { CompanyCrudComponent } from './views/company-crud/company-crud.component'
import { CompanyCreateComponent } from './components/company/company-create/company-create.component';

import { PartnerCrudComponent } from './views/partner-crud/partner-crud.component'
import { PartnerCreateComponent } from './components/partner/partner-create/partner-create.component';

const routes: Routes = [
  {
    path:"",
    component: HomeComponent
  
  },
  {
    path:"companies",
    component: CompanyCrudComponent
  },
  {
    path:"companies/:id/show",
    component: CompanyShowComponent
    
  },
  {
    path:"companies/create",
    component: CompanyCreateComponent
    
  },
  {
    path:"companies/:id/update",
    component: CompanyUpdateComponent
    
  },
  {
    path:"companies/:id/delete",
    component: CompanyDeleteComponent
    
  },
  {
    path:"companies/:id/showPartners",
    component: CompanyShowPartnersComponent
  },
  {
    path:"companies/:id/addPartner",
    component: CompanyAddPartnerComponent
  },
  {
    path:"partners",
    component: PartnerCrudComponent
  },
  {
    path:"partners/:id/show",
    component: PartnerShowComponent

  },
  {
    path:"partners/create",
    component: PartnerCreateComponent

  },
  {
    path:"partners/:id/update",
    component: PartnerUpdateComponent
    
  },
  {
    path:"partners/:id/delete",
    component: PartnerDeleteComponent
    
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
