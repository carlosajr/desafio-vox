# Desafio VOX

Sistema de cadastro de empresas e o seu quadro societário desenvolvido para o Desafio Vox

Acesse uma demonstração pelo link http://desafiovox.cvmakers.com.br/

A API também está disponivel através do link http://api.desafiovox.cvmakers.com.br:8000

# Instalação

O prcesso de instalação pe divido em duas partes o Backend e o Frontend

## BackEnd

Na pasta do Backend Execute o comando

```
composer install
```

Configure o banco de dados no arquivo .env

```
DATABASE_URL="postgresql://usuario:senha*@127.0.0.1:5432/nomeDoDb?serverVersion=13&charset=utf8"
```

Execute o comando para criar o banco

```
php bin/console doctrine:database:create
```

Execute os comandos para criar as tabelas

```
php bin/console doctrine:migrations:migrate
```

Após configuração do Banco, deve iniciar oservidor do Symfony

```
symfony serve -d -no-tls
```

O servidor da API iniciará na porta 8000

## FrontEnd

Entre na pasta frontend e execute o comando para instalar as dependências.

```
npm install
```

É necessario configurar os endereços padrões para as requisiçoes nos arquivos de Serviços.

Edite a variavel baseUrl em app/components/company/company.service.ts

```js
baseUrl = "http://desafiovox.cvmakers.com.br:8000/companys";
```

Edite a variavel baseUrl em app/components/partner/partner.service.ts

```js
baseUrl = "http://desafiovox.cvmakers.com.br:8000/partners";
```

Lógo após utilize este comando para rodar o projeto.

```
npm start
```

As dependencias serão instaladas e logo ele irá subir o servidor HTTP na porta 4200

# Desenvolvimento

## BackEnd

O BackEnd foi desenvolvido utilizando A versão mais recente do Framework Symfony com a ORM Doctrine utilizando O banco de dados PostgreSQL.

Foi criada um API REST utilizando os Verbos GET, POST, PUT, PATCH E DELETE

As entidades foram estabelecidas com um relacionamento ManyToMany

Todos os controller foram desenvolvidos utilizando as funções nativas de conxeão da ORM Doctrine

Utilizando um Normalizer para contornar o problema de Referência circular fruto do relacionamento das entidades, encontrado na serialização do objeto.

Utilizando o bundle de CORS do Nelmio para configuração das permissões das origens das requisições

- Encaminhamentos:
  - Desenvolvimento do EndPoint de Auntenticação com JWT
  - Desenvolvimento de validações para os Controllers

## FrontEnd

O FrontEnd foi desenvolvido utilizando a versão mais recente do framework solicitado, o Angular 9.

Foi utilizado o sistema de Templates e Styles Offline para melhor compreensão e escalabilidade do projeto

Foi desenvolvido o CRUD de Empresas e CRUD de Sócios e o estabelecimento de vinculos entres eles por meios dos Componentes, Serviços, Modelos e Templates.

- Encaminhamentos:
  - Desenvolvimento do Componente de Login
  - Implementação de mascaras e validações nos Formulários
  - Utilização de API para agilizar os cadastros (Consulta CNPJ e CEP)
